import React, {useEffect, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Avatar from '@material-ui/core/Avatar';
import {hashCode, intToRGB, restoreLineBreaks} from "../../Common";



export default function Messages({ messages, myName }) {
    const classes = useStyles();


    const messagesEndRef = useRef(null)

    const scrollToBottom = () => {
        messagesEndRef.current.scrollIntoView({behavior: "smooth",block: "end"})
    }

    useEffect(scrollToBottom, [messages]);


    return (
        <React.Fragment>
            <CssBaseline />
            <Paper square className={classes.paper}>
                <List className={classes.list} >

                    {messages.map(({ id, user, text, date,time, person }, i, arr) => (
                        <React.Fragment key={i}>
                            {
                                ((i>0 && arr[i - 1].date !== date) || i===0) &&
                                <ListSubheader className={classes.subheader}>
                                        <div className={classes.dateLast}>{date}</div>
                                </ListSubheader>
                            }




                            {
                                user !== "Admin" &&
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar style={{
                                            backgroundColor: intToRGB(hashCode(user)).toString(),
                                            border: '3px solid #4457bf24'
                                        }} alt={user.toUpperCase()} src={user}/>
                                    </ListItemAvatar>
                                    <ListItemText primary={user} secondary={restoreLineBreaks(text)}
                                                  className={user.toLowerCase() === myName.toLowerCase() ? classes.myMessage : classes.listItem}/>
                                    <span className={classes.time}>{time}</span>
                                </ListItem>
                            }

                            {
                                user === "Admin" &&
                                <ListItem>
                                    <ListItemText  secondary={text}
                                                  className={classes.admin}/>
                                    <span className={classes.time}>{time}</span>
                                </ListItem>
                            }
                        </React.Fragment>
                    ))}

                </List>
                <span style={{position:'relative'}}>
                <i style={{position:'absolute',bottom:'-50px'}} ref={messagesEndRef}/>
                </span>
            </Paper>

        </React.Fragment>
    );


}



const useStyles = makeStyles((theme) => ({
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    paper: {
        overflowY:'scroll',
        overflowX:'hidden',
        height:'100%',
        marginTop: '64px',
        [theme.breakpoints.down('xs')]: {
            marginTop: '57px',
        },
    },
    list: {
        marginBottom: theme.spacing(2),
    },
    listItem: {
        wordWrap:'break-word',
    },
    subheader: {
        backgroundColor: 'transparent',
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    grow: {
        flexGrow: 1,
    },
    fabButton: {
        position: 'absolute',
        zIndex: 1,
        top: -30,
        left: 0,
        right: 0,
        margin: '0 auto',
    },
    time: {
        color:'#999',
        fontSize:'12px',
        position:'absolute',
        bottom: '-3px',
        right: '20px'
    },
    myMessage:{
        color: '#0027ff',
        fontWeight: 'bold',
        wordWrap:'break-word',
    },
    dateLast:{
        backgroundColor: theme.palette.background.paper,
        paddingTop: '3px 10px 3px 10px',
        borderBottomRightRadius: '10px',
        borderBottomLeftRadius: '10px',
        boxShadow: '0px 2px 2px #EEE',
        width: '134px',
        margin: '0 auto',
        marginTop: '0px',
        lineHeight: '22px',

    },
    dateLastOuter:{
        backgroundColor: theme.palette.background.paper,
        paddingBottom: '11px',
        paddingLeft: '16px',
        paddingRight: '16px',
        borderBottomRightRadius: '10px',
        borderBottomLeftRadius: '10px',
    },
    admin:{
        textAlign:'center',
    }
}));

/*
const messages = [
    {
        id: 1,
        date: '28 February 2018',
        time: '14:25',
        primary: 'Arturs',
        secondary: "I'll be in the neighbourhood this week. Let's grab a bite to eat",
        person: '/static/images/avatar/5.jpg',
    },
    {
        id: 2,
        date: '1 May 2019',
        time: '14:29',
        primary: 'Anastasija',
        secondary: `Do you have a suggestion for a good present for John on his work
      anniversary. I am really confused & would love your thoughts on it.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 3,
        date: '19 October 2019',
        time: '15:05',
        primary: 'Arturs',
        secondary: 'I am try out this new BBQ recipe, I think this might be amazing',
        person: '/static/images/avatar/2.jpg',
    },
    {
        id: 4,
        date: '19 October 2019',
        time: '15:12',
        primary: 'Arturs',
        secondary: 'I have the tickets to the ReactConf for this year.',
        person: '/static/images/avatar/3.jpg',
    },
    {
        id: 5,
        date: '19 October 2019',
        time: '15:15',
        primary: "Mihails",
        secondary: 'My appointment for the doctor was rescheduled for next Saturday.',
        person: '/static/images/avatar/4.jpg',
    },
    {
        id: 6,
        date: '19 October 2019',
        time: '16:20',
        primary: 'Mihails',
        secondary: `Menus that are generated by the bottom app bar (such as a bottom
      navigation drawer or overflow menu) open as bottom sheets at a higher elevation
      than the bar.`,
        person: '/static/images/avatar/5.jpg',
    },
    {
        id: 7,
        date: '20 October 2019',
        time: '00:11',
        primary: 'Anastasija',
        secondary: `Who wants to have a cookout this weekend? I just got some furniture
      for my backyard and would love to fire up the grill.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 8,
        date: '20 October 2019',
        time: '03:20',
        primary: 'Arturs',
        secondary: `Yohoho i will do this at home now.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 9,
        date: '20 October 2019',
        time: '09:45',
        primary: 'Andrejs',
        secondary: `I will not do this. Safety first. Trololo tralivali say something, i need long test`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 10,
        date: '20 October 2019',
        time: '11:01',
        primary: 'Andrejs',
        secondary: `Do not test this to much`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 11,
        date: '20 October 2019',
        time: '12:25',
        primary: 'Andrejs',
        secondary: `I want to write something here. Why not?`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 12,
        date: '21 October 2019',
        time: '11:32',
        primary: 'Arturs',
        secondary: `Do you have a suggestion for a good present for John on his work
      anniversary. I am really confused & would love your thoughts on it.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 13,
        date: '21 October 2019',
        time: '11:33',
        primary: 'Anastasija',
        secondary: 'I am try out this new BBQ recipe, I think this might be amazing',
        person: '/static/images/avatar/2.jpg',
    },
    {
        id: 14,
        date: '22 October 2019',
        time: '12:05',
        primary: 'Edgard',
        secondary: 'I have the tickets to the ReactConf for this year.',
        person: '/static/images/avatar/3.jpg',
    },
    {
        id: 15,
        date: '22 October 2019',
        time: '12:31',
        primary: "Alik",
        secondary: 'My appointment for the doctor was rescheduled for next Saturday.',
        person: '/static/images/avatar/4.jpg',
    },
    {
        id: 16,
        date: '22 October 2019',
        time: '12:52',
        primary: 'Alik',
        secondary: `Menus that are generated by the bottom app bar (such as a bottom
      navigation drawer or overflow menu) open as bottom sheets at a higher elevation
      than the bar.`,
        person: '/static/images/avatar/5.jpg',
    },
    {
        id: 17,
        date: '22 October 2019',
        time: '14:16',
        primary: 'Arturs',
        secondary: `Who wants to have a cookout this weekend? I just got some furniture
      for my backyard and would love to fire up the grill.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 18,
        date: '22 October 2019',
        time: '19:22',
        primary: 'Edgars',
        secondary: `Yohoho i will do this at home now.`,
        person: '/static/images/avatar/1.jpg',
    },
    {
        id: 19,
        date: '22 October 2019',
        time: '22:40',
        primary: 'Anastasija',
        secondary: `I will not do this. Safety first. Trololo tralivali say something, i need long test`,
        person: '/static/images/avatar/1.jpg',
    },
];*/