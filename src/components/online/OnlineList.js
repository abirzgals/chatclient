import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    h6: {
        textAlign: 'center'
    }
});

export default function OnlineList({users}) {
    const classes = useStyles();
    return (
        <React.Fragment key={"right"}>
            <Typography className={classes.h6} variant="h6" gutterBottom>
                {users.length>0 && <span>Online</span>}
            </Typography>
            <Divider />
            <List>
            {users && users.map(({ name }, i, arr) => (
                <ListItem button key={name}>
                    <ListItemText primary={name} />
                </ListItem>
            ))}
            </List>
        </React.Fragment>
    )

}
