import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import OnlineList from "./OnlineList";

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    h6: {
        textAlign: 'center'
    }
});



export default function Online({users,myName}) {
    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    return (
        <React.Fragment key={"left"}>
            <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
                onClick={toggleDrawer('left', true)}
            >
                <MenuIcon/>
            </IconButton>

            <Drawer anchor={"left"} open={state["left"]} onClose={toggleDrawer("left", false)}>

                <div
                    className={classes.list}
                    onClick={toggleDrawer("left", false)}
                    onKeyDown={toggleDrawer("left", false)}
                >
                    <div className={classes.toolbar} />
                    <OnlineList users={users} myName={myName}/>
                </div>

            </Drawer>

        </React.Fragment>
    );
}