import React, {useEffect, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import SendIcon from '@material-ui/icons/Send';
import InputAdornment from "@material-ui/core/InputAdornment";
import FilledInput from "@material-ui/core/FilledInput";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import {Grid} from "@material-ui/core";
import Container from "@material-ui/core/Container";


export default function SendMessage({ setMessage, sendMessage, message, focusTrigger }) {
    const classes = useStyles();

    //return focus to input after submitted
    const inputEl = useRef(null);
    const onButtonClick = (e) => {
        sendMessage(e);
        inputEl.current.focus();
    };

    const onEnterPress = (e) => {
        if(e.keyCode === 13 && e.shiftKey === false) {
            e.preventDefault();
            sendMessage(e);
        }
    }



    useEffect(() => {
        if (focusTrigger!=="") inputEl.current.focus();
    }, [focusTrigger]);

    return (
        <React.Fragment>
            <CssBaseline />


                <Grid container className={classes.grid}>

                    <Container maxWidth="xl" className={classes.container}>
                        <Grid container>

                    <Grid item xs={false} md={2}/>


                    <Grid item xs={12} md={8}>

                        <Paper elevation={0} className={classes.paperBg}>
                            <FormControl className={classes.form} variant="filled">
                                <InputLabel htmlFor="filled-send-message">Enter message</InputLabel>
                                <FilledInput
                                    className={classes.input}
                                    style={{backgroundColor: "white"}}
                                    id="input-send-message"
                                    multiline={true}
                                    rowsMax={10}
                                    onChange={({target: {value}}) => setMessage(value.substring(0,1000))}
                                    onKeyDown={e=>onEnterPress(e)}
                                    value={message}
                                    inputRef={inputEl}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                className={classes.sendButton}
                                                onClick={e => onButtonClick(e)}
                                                disabled={!message}
                                                aria-label={"Send"}
                                                size={"medium"}
                                                edge="end"

                                            >
                                                <SendIcon color="primary"/>
                                            </IconButton >
                                        </InputAdornment>
                                    }
                                />
                            </FormControl>
                        </Paper>

                    </Grid>

                    <Grid item xs={false} md={2}/>
                    </Grid>

                </Container>

                </Grid>

        </React.Fragment>
    );
}



const useStyles = makeStyles((theme) => ({
    text: {
        padding: theme.spacing(2, 2, 0),
    },
    sendButton: {
        zIndex: 1,
        top: -7,
        left: 0,
        right: 0,
        margin: '0 auto',
    },
    grid: {
        'top':'auto',
        'bottom':'0px',
        'left': 'auto',
        'right': '0',
        'position': 'fixed',
        zIndex: 2,
    },
    paperBg:{
        borderRadius:'0px',
        boxShadow:'0px -5px 10px rgba(0,0,0,0.12)',
        borderTop:'rgba(0, 0, 0, 0.87)',
        paddingBottom:'0px',
    },
    input:{
        borderRadius:'0px',
        backgroundColor: "white",
    },
    form:{
        'width':'100%',
    },
    container:{
        paddingLeft:'0px',
        paddingRight:'0px',
    },
}));
