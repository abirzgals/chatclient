import React, {useEffect, useRef, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import {AccountCircle} from "@material-ui/icons";
import {hashCode, intToRGB} from "../../Common";



export default function Profile({myName,setName, setRoom, error,setError,connectAttempt,setConnectAttempt, connected, logout}) {
    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const [editName, setEditName] = useState('');
    const [profileOpen, setProfileOpen] = useState(true);
    const changeName = (value) =>{
        //if (error==="" && myName!==value)
        //    setError('connecting...');
        setError("");
        setName(value);
        setRoom('general');
        setConnectAttempt(connectAttempt+1);
        setProfileOpen(false);
    }
    const onEnterPress = (e) => {
        if(e.keyCode === 13) {
            e.preventDefault();
            changeName(editName);
        }
    }

    const toggleDrawer = (anchor, open) => (event) => {
        //if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
        //    return;
        //}
        setProfileOpen(open);
        setState({ ...state, [anchor]: open });
    };


    useEffect(() => {
        if (editName) {
            setEditName(editName.trim().replace(/[^a-zа-я0-9_]/gi,'').substring(0,25));
        }
    }, [editName]);

    useEffect (()=>{
        //open on start
        toggleDrawer("right", true)
    },[]);

    useEffect (()=>{
        //close only if no errors
        if (error==="" && profileOpen===false)
            setState({ ...state, ["right"]: false });
        if (error!=="") {
            setState({...state, ["right"]: true});
        }

    },[error,profileOpen]);


    //{setRoom('general')}

    return (
        <React.Fragment key={"right"}>

            <Avatar
                onClick={toggleDrawer('right', true)}
                style={{
                    backgroundColor:intToRGB(hashCode(myName)).toString(),
                    border: '3px solid #4457bf24'
            }} alt={myName.toUpperCase()} src={myName} />
            <Button
                color="inherit"
                aria-label="profile"
                onClick={toggleDrawer('right', true)}
                className={classes.profileButton}
            >{myName}</Button>

            <Drawer
                anchor={"right"}
                open={state["right"]}
                onClose={toggleDrawer("right", false)}
                disableBackdropClick={connected===false}
            >

                <div
                    className={classes.list}
                    //onClick={toggleDrawer("right", false)}
                    //onKeyDown={toggleDrawer("right", false)}
                >
                    <div className={classes.toolbar} />
                    <Typography  className={classes.h6} variant="h6" gutterBottom>
                        Profile
                    </Typography>
                    <Divider />

                    <FormControl className={classes.margin}>
                        {connected===false &&

                        <React.Fragment>
                        <InputLabel htmlFor="input-with-icon">Nickname</InputLabel>
                        <Input
                            className={classes.margin}
                            id="input-with-icon"
                            value={editName}
                            onChange={({target: {value}}) => setEditName(value)}
                            onKeyDown={e=>onEnterPress(e)}
                            inputProps={{
                                'aria-label': 'weight',
                            }}
                            startAdornment={
                                <InputAdornment position="start">
                                    <AccountCircle />
                                </InputAdornment>
                            }
                        />


                        <Typography variant="body2" style={{color: 'red', margin: '0px', padding: '0px'}}>
                            {error}
                        </Typography>


                        <Button
                            className={classes.margin}
                            variant="contained"
                            color="primary"
                            enabled={false}
                            onClick={() => changeName(editName)}
                        >Join Chat</Button>
                        </React.Fragment>
                        }

                        {connected===true &&
                        <React.Fragment>
                            <Typography variant="h5" component="h5">
                            {editName}
                            </Typography>
                            <Button
                                className={classes.margin}
                                variant="contained"
                                color="secondary"
                                onClick={() => logout()}
                            >Logout</Button>
                        </React.Fragment>
                        }

                    </FormControl>

                </div>

            </Drawer>

        </React.Fragment>
    );
}



const useStyles = makeStyles({
    list: {
        width: 250,
        textAlign: 'center',
    },
    fullList: {
        width: 'auto',
    },
    margin:{
        margin:'10px'
    },
    h6: {
        textAlign: 'center'
    },
    profileButton:{
        textTransform: 'none',
    }
});
