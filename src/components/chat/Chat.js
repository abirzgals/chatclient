import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import io from "socket.io-client";
import {Grid} from "@material-ui/core";
import Header from "../header/Header";
import Messages from "../messages/Messages";
import SendMessage from "../sendmessage/SendMessage";
import Container from "@material-ui/core/Container";
import OnlineList from "../online/OnlineList";
import Hidden from "@material-ui/core/Hidden";




let socket;
const Chat = ({endpoint}) => {
    const classes = useStyles();

    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [error, setError] = useState([]);
    const [connectAttempt, setConnectAttempt] = useState(0);
    const [connected, setConnected] = useState(false);
/*
    useEffect (()=>{
        if (error!=="") {
            setConnectAttempt(0);
            setConnected(false);
        }
    },[error,connectAttempt] );
*/

    function logout(){
        socket.emit('disconnect');
        socket.off();
        socket.close();
        setConnectAttempt(0);
        setConnected(false);
        setMessages([]);
        setUsers([]);
    }

    useEffect (()=>{
        //const name = 'User_'+Math.floor(Math.random()*100);
        //const room = 'general';
        if (connectAttempt>0) {
            if (socket)
                socket.close();
            socket = io(endpoint);
            setName(name);
            setRoom(room);

            socket.emit('join', {name, room}, (error) => {
                if (error) {
                    setError(error);
                    setConnected(false);
                    setMessages([]);
                    setUsers([]);
                } else {
                    setError("");
                    setConnected(true);
                }
            });
        }
        return () =>{
            if (connectAttempt>0) {
                socket.emit('disconnect');
                socket.off();
                setConnectAttempt(0);
                setConnected(false);
                setMessages([]);
                setUsers([]);
            }
        }
    },[endpoint, name, room, connectAttempt] );

    useEffect (()=>{
        if (connectAttempt>0) {
            socket.on('message', message => {
                setMessages(messages => [...messages, message]);
            });

            socket.on("roomData", ({users}) => {
                setUsers(users);
            });

            socket.on("connect_error", ({users}) => {
                setError("server unavailable");
                setConnectAttempt(0);
                setMessages([]);
                setUsers([]);
                socket.off();
            });

            socket.on("disconnect_activity", () => {
                setError("disconnected due inactivity");
                setConnectAttempt(0);
                setConnected(false);
                socket.emit('disconnect');
                socket.off();
                setMessages([]);
                setUsers([]);
            });
        }
    }, [name,room, connectAttempt]);

    const sendMessage = (event) => {
        event.preventDefault();
        if(message) {
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    }

    return (
        <Container maxWidth="xl" style={{paddingLeft:'0px',paddingRight:'0px'}}>
            <Grid item xs={12} md={8}>
                <Header users={users} myName={name} setName={setName} setRoom={setRoom} error={error} setError={setError} connectAttempt={connectAttempt} setConnectAttempt={setConnectAttempt} connected={connected} logout={logout}/>
            </Grid>
            <Grid container>
                <Grid item xs={false} md={2} className={classes.leftContainer}>
                    {connected &&
                    <Hidden smDown>
                        <OnlineList users={users} myName={name}/>
                    </Hidden>
                    }
                </Grid>
                <Grid item xs={12} md={8} className={classes.messages} >
                    {connected &&
                        <Messages messages={messages} myName={name}/>
                    }
                </Grid>
                <Grid item xs={false} md={2}/>
            </Grid>
            {connected &&
                <SendMessage message={message} setMessage={setMessage} sendMessage={sendMessage} focusTrigger={name}/>
            }
        </Container>
    );

}





const useStyles = makeStyles((theme) => ({
    messages: {
        height: '100vh',
        overflow: 'hidden',
        paddingBottom:'120px',
        [theme.breakpoints.down('xs')]: {
            paddingBottom: '113px',
        },
    },
    leftContainer:{
        marginTop:'74px',
    }
}));



export default Chat;