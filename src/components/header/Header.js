import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Online from "../online/Online";
import Profile from "../profile/Profile";
import Hidden from "@material-ui/core/Hidden";
import {Grid} from "@material-ui/core";


function Header({users, myName, setName, setRoom, error, setError, connectAttempt,setConnectAttempt, connected, logout}) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar >
                <Toolbar>
                    <Grid item xs={4} style={{display:'flex'}}>
                        <Hidden mdUp>
                            <Online users={users} myName={myName}/>
                        </Hidden>
                    </Grid>
                    <Grid item xs={4} style={{display:'flex'}}>
                        <Typography variant="h6" className={classes.title}>
                            {users && users.length>0 && users.length}
                            {connected===true && error==="" ? <span> online</span> : <span></span>}
                        </Typography>
                    </Grid>
                    <Grid item xs={4} style={{display:'flex', justifyContent:'flex-end'}}>
                        <Profile myName={myName} setName={setName} setRoom={setRoom} error={error} setError={setError} connectAttempt={connectAttempt} setConnectAttempt={setConnectAttempt} connected={connected} logout={logout}/>
                    </Grid>




                </Toolbar>
            </AppBar>
        </div>
    );
}



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper:{
        width: '50%',
        maxWidth: '400px',
        padding:'10px',
    },
    drawerPaper2:{
        width: '16%',
        maxWidth: '400px',
        padding:'10px',
    },
}));

export default Header;



























/*

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from "@material-ui/core/Avatar";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";



export default function Header() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <AppBar>
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="menu"

                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        6 persons in Chat
                    </Typography>
                    <Avatar style={{
                        backgroundColor:"#67f853b3",
                        border: '3px solid #4457bf24'
                    }} alt={"Arturs"} src={"url"} />

                    <Button color="inherit">Arturs</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}




const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));
*/