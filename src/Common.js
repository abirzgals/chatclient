import React from "react";

export function hashCode(str) { // java String#hashCode
    if (!str) return 0;
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

export function intToRGB(i){
    let c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();
    return "#"+c+"b3";
}

export function restoreLineBreaks(str){
    return str.split('\n').map((item, index) => {
        return (index === 0) ? item : [<br key={index} />, item]
    })
}
