import io from 'socket.io-client';
import SocketWorker from './utilities/SocketWorker';

function Socket({endpoint}) {
    this.socket = io.connect(endpoint);

    this.socket.on('statusChange', (data) => {
        return SocketWorker.receiveOrderStatusChange(data);
    })
};

const socket = new Socket();

export  { socket };