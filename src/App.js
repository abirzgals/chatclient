import React from 'react';
import './App.css';
import Chat from "./components/chat/Chat";

function App() {

  //const ENDPOINT = 'localhost:3001';
  //const ENDPOINT = '192.168.0.100:3001';
  const ENDPOINT = 'resonance.id.lv:3001';

  return (
    <div className="App">
      <Chat endpoint={ENDPOINT}/>
    </div>
  );
}

export default App;
